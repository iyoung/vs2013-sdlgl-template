#include "SDL.h"
// glew extension wrangler
#include "GL\glew.h"
// iostream for console output
#include <iostream>
//GL templated math library, compatible with opengl
#include <glm\glm.hpp>
//SDL image library for loading textures
#include <SDL_image.h>
//SDL true type font library for loading fonts to create onscreen text 
#include <SDL_ttf.h>
//SDL sound library, for playing music etc, if it's needed
#include <SDL_mixer.h>

//simple error function, can be tied into exception handling
// NO GLOBALS. EVER!!!

void exitFatalError(const char *message) {
	std::cout << message << " ";
	exit(1);
}
void init()
{
	//create & init app objects here
}
bool handleEvent(SDL_Event &event)
{
	//event handling stuff goes here
	//if exit condition met
	//return false
	//else
	return true;
}

void update(float deltaTime)
{
	//any scene updates go here
}

void draw(SDL_Window *window)
{
	glClearColor(0.5f, 0.5f, 0.5f, 1.0f);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	//draw calls go here
	SDL_GL_SwapWindow(window); // swap buffers
}

SDL_Window * setupRC(SDL_GLContext &context) {
	SDL_Window * window;
	if (SDL_Init(SDL_INIT_VIDEO) < 0) // Initialize video
		exitFatalError("Unable to initialize SDL");

	// Request an OpenGL 3.0 context.

	SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 3);
	SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 3);
	SDL_GL_SetAttribute(SDL_GL_CONTEXT_PROFILE_MASK, SDL_GL_CONTEXT_PROFILE_CORE);

	SDL_GL_SetAttribute(SDL_GL_DOUBLEBUFFER, 1);  // double buffering on
	SDL_GL_SetAttribute(SDL_GL_ALPHA_SIZE, 8); // 8 bit alpha buffering
	SDL_GL_SetAttribute(SDL_GL_MULTISAMPLEBUFFERS, 1);
	SDL_GL_SetAttribute(SDL_GL_MULTISAMPLESAMPLES, 4); // Turn on x4 multisampling anti-aliasing (MSAA)

	// Create 800x600 window
	window = SDL_CreateWindow("SDL/GLM/OpenGL Demo", SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED,
		800, 600, SDL_WINDOW_OPENGL | SDL_WINDOW_SHOWN);
	if (!window) // Check window was created OK
		exitFatalError("Unable to create window");

	context = SDL_GL_CreateContext(window); // Create opengl context and attach to window
	SDL_GL_SetSwapInterval(1); // set swap buffers to sync with monitor's vertical refresh rate
	return window;
}

int main(int argc, char** argv)
{
	SDL_Init(SDL_INIT_EVERYTHING);
	SDL_Window * hWindow; // window handle
	SDL_GLContext glContext; // OpenGL context handle
	hWindow = setupRC(glContext); // Create window and render context 

	// Required on Windows... init GLEW to access OpenGL beyond 1.1// remove on other platforms
	glewExperimental = true;
	//init glew for opengl calls
	GLenum err = glewInit();
	if (GLEW_OK != err)
	{// glewInit failed, something is seriously wrong.
		std::cout << "glewInit failed, aborting." << std::endl;
		exit(1);
	}
	std::cout << glGetString(GL_VERSION) << std::endl;
	bool running = true;	
	float deltaTime = 0.0f;
	SDL_Event sdlEvent;  // variable to detect SDL events
	init();
	while (running)
	{
		while (SDL_PollEvent(&sdlEvent)) {
			if (sdlEvent.type == SDL_QUIT)
				running = false;
			else
				handleEvent(sdlEvent);
		}
		update(deltaTime / 1000.0f);
		draw(hWindow);
	}
	SDL_Quit();

	return 0;
}